#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include "common.h"
#include "line_parser.h"

#define MAX_INPUT_SIZE 2048
#define LS_RESP_SIZE 2048
#define EXIT_SUCCESS 0
#define EQUAL_STR 0

/*================================(Var. Deff.)=====================*/

client_state *state;                /*a struct to store client state , see common.h */
char user_input[MAX_INPUT_SIZE];    /*buffer to hold user input */
cmd_line* parsed_input;             /*struc to hold parsed command , see line_parser.h */
struct addrinfo hints;              /*the hints parameter points to a struct addrinfo */

int debug_flag = 0;
char port_num[5] = "2018";
char hello_str[6] = "hello";
char _recieve[101];
char _send[101];
char server_address[17];

/*================================(Func. Deff.)====================*/

int exec(cmd_line* command);                     /*a function that executes the input command */
void init();                                     /*initialize client_state */
void quit();                                     /*frees data on heap and exits */
int connect_to_server();                         /*tries to connect client to server */
int disconnect_from_server();                    /*disconnects client from server */
void debug_mode( int error);                     /*debug mode : print all errors*/
int get_listed_files();                         /*get ls from server*/

/*===============================(Main loop)=======================*/

int main(int argc , char** argv){
    if( argc > 1 && (strcmp(argv[1],"-d") == EQUAL_STR)) debug_flag = 1;
    init();
    while( printf("server:%s>", state->server_addr ) ){
        fgets( user_input, MAX_INPUT_SIZE, stdin );
        parsed_input = parse_cmd_lines( user_input );
        
        if(parsed_input){
            exec( parsed_input);
        }
        free_cmd_lines( parsed_input );
    }
    return 0;
}

/*==============================(Impl.)=============================*/

int get_listed_files(){
    debug_mode( send(state->sock_fd, "ls", 3, 0) );
    debug_mode( recv(state->sock_fd, _recieve, 3, 0) );
    if( strcmp(_recieve,"ok") == EQUAL_STR ){
        debug_mode( recv(state->sock_fd, _recieve, LS_RESP_SIZE, 0) );
        printf("ls :\n%s\n",_recieve);
        return 0;
    }
    else{
        printf("Server Error: %s\n",_recieve);
        disconnect_from_server();
        return -1;
    }
}


void debug_mode( int error){
    if( debug_flag == 0 ) return;
    if( strcmp(gai_strerror(error),"Unknown error") == EQUAL_STR) return;
    fprintf(stderr, "%s|Log: %s\n",state->server_addr, gai_strerror(error));
}

void init(){
    if( state == NULL ){
        state = malloc( sizeof(client_state) );
        state->server_addr = malloc( sizeof(char) * 16 );
    }
    strcpy( state->server_addr ,"nil" );
    state->conn_state = IDLE;
}

void quit(){
    free_cmd_lines( parsed_input );
    free( state->server_addr );
    free( state->client_id );
    free( state );
    exit( EXIT_SUCCESS );
}

int connect_to_server(){
    struct addrinfo *res;
    int temp_socket_fd;
    int status;
    int i = 0;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC; 
    hints.ai_socktype = SOCK_STREAM;
    
    debug_mode( getaddrinfo( parsed_input->arguments[1] ,port_num , &hints, &res) );
    debug_mode( (temp_socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) );
    debug_mode( (status = connect(temp_socket_fd, res->ai_addr, res->ai_addrlen)) );
    if(status != 0){
        disconnect_from_server();
        return -1;
    }
    debug_mode( send(temp_socket_fd, hello_str, 6, 0) );
    debug_mode( recv(temp_socket_fd, _recieve, 101, 0) );

    state->client_id = malloc( sizeof(char) * 24);
    while(_recieve[i] != '\0'){
        if( (i<5) && (_recieve[i] == hello_str[i]) )i++; 
        else if( (i == 5) && (_recieve[i] == ' ') ) i++;
        else{
            if( i < 6 ){
                printf("WRONG RESPONSE!\n");
                state->conn_state = IDLE;
                close(temp_socket_fd);
                free( state->client_id );
                return -1;
            }
            else{
                state->client_id[i-6] = _recieve[i];
            }
            i++;
        }
    }
    
    state->conn_state = CONNECTED;
    state->sock_fd = temp_socket_fd;
    strcpy(state->server_addr , parsed_input->arguments[1]);
    
    return printf("%s\n",_recieve);
}

int disconnect_from_server(){
    send( state->sock_fd, "bye" , 4, 0);
    close( state->sock_fd );
    state->sock_fd = -1;
    free( state->client_id );
    state->client_id = NULL;
    strcpy( state->server_addr , "nil" );
    state->conn_state = IDLE;
    return 0;
}


int exec(cmd_line* command){
    
    if( strcmp(command->arguments[0],"quit") == EQUAL_STR ){
        quit();
    }
    else if( strcmp(command->arguments[0],"conn") == EQUAL_STR ){
        if( (state->conn_state) == IDLE ){
            state->conn_state = CONNECTING;
            return connect_to_server();
        }
        else return -2;
    }
    else if( strcmp(command->arguments[0],"bye") == EQUAL_STR ){
        if( (state->conn_state) == CONNECTED){
            return disconnect_from_server();
        }
        else return -2;
    }
    else if( strcmp(command->arguments[0],"ls") == EQUAL_STR ){
        if( (state->conn_state) == CONNECTED ){
            return get_listed_files();
        }
        else return -2;
    }
    else{
        debug_mode( send(state->sock_fd, parsed_input->arguments[0], strlen( parsed_input->arguments[0]), 0) );
        debug_mode( recv(state->sock_fd, _recieve, 101, 0) );
        if( strcmp(_recieve,"nok unknown command") == EQUAL_STR ){
            printf("Server Error: %s\n","unknown command");
            disconnect_from_server();
        }
    }
    return 0;
}