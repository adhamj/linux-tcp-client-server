
all: client server


server: server.o common.o line_parser.o
	gcc -g -Wall -o server server.o common.o line_parser.o

server.o: server.c common.h line_parser.h
	gcc -g -Wall -c -o server.o server.c
	
client: client.o common.o line_parser.o
	gcc -g -Wall -o client client.o common.o line_parser.o
	
client.o: client.c common.h line_parser.h
	gcc -g -Wall -c -o client.o client.c
	
common.o: common.c
	gcc -g -Wall -c -o common.o common.c

line_parser.o: line_parser.c
	gcc -g -Wall -c -o line_parser.o line_parser.c

#tell make that "clean" is not a file name!
.PHONY: clean

#Clean the build directory
clean: 
	rm -f *.o client
	rm -f *.o server

 
