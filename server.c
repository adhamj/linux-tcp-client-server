#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include "common.h"
#include "line_parser.h"
#include <limits.h>

#define MAX_INPUT_SIZE 2048
#define LS_RESP_SIZE 2048
#define EXIT_SUCCESS 0
#define RUNNING 1
#define EQUAL_STR 0
#define PORT_NUM 2018

/*================================(Var. Deff.)=====================*/

client_state *client;                /*a struct to store client state , see common.h */
cmd_line* parsed_input;              /*struc to hold parsed command , see line_parser.h */
struct addrinfo hints;               /*the hints parameter points to a struct addrinfo */
struct sockaddr_in name;
struct sockaddr_storage their_addr;
socklen_t addr_size;

int socket_fd;
int debug_flag = 0;
char id[16] ="0";
char port_num[5] = "2018";
char hello_str[7] = "hello ";
char _recieve[101];
char _send[101];
char server_address[17];
char path_name[PATH_MAX];
char* listed_files;
/*================================(Func. Deff.)====================*/

int exec(cmd_line* command);        /*a function that executes the input command */
void init();                        /*initialize server */
void quit();                        /*frees data on heap and exits */
void disconnect_client();           /*disconnect client from server*/
void handle_client(char* recieved); /*handle client request*/
void debug_mode(char* _recieve);    /*debug mode : print all incoming messages*/
void list_files();                  /*send client CWD files*/

/*===============================(Main loop)=======================*/

int main(int argc , char** argv){
    //debug mode
    if( argc > 1 && (strcmp(argv[1],"-d") == EQUAL_STR)) debug_flag = 1;
    
    //initialize needed data
    init();
    
    //main while loop
    while( RUNNING ){
        client->sock_fd = accept(socket_fd, (struct sockaddr *)&their_addr, &addr_size);
        if(client->sock_fd > -1){
            client->conn_state = CONNECTED;
            id[0]++;
            strcpy(client->client_id,id);
            while(client->conn_state == CONNECTED) {
                memset(&_recieve, 0, sizeof hints);
                recv(client->sock_fd, _recieve, 101, 0);
                debug_mode(_recieve);
                handle_client(_recieve);
            }
        }
        else disconnect_client();
    }
    return 0;
}
/*==============================(Impl.)=============================*/

void list_files(){
    getcwd(path_name,PATH_MAX);
    if ( (listed_files = list_dir()) == NULL){
        printf("asdasd\n");
        send(client->sock_fd,"nok filesystem",15,0);
        disconnect_client();
        return;
    }
    send(client->sock_fd,"ok",3,0);
    printf("Listed files at %s\n",path_name);
    send(client->sock_fd,listed_files,strlen(listed_files),0);
}



void debug_mode( char* _recieve){
    if( debug_flag == 0 ) return;
    printf("%s|Log: %s\n",client->server_addr,_recieve);
}

void handle_client(char* recieved){
    if( strcmp(recieved,"hello") == EQUAL_STR ){
        strcat(hello_str,id);
        send(client->sock_fd,hello_str,8,0);
        
    }
    else if( strcmp(recieved,"bye") == EQUAL_STR ){
        send(client->sock_fd,"bye",4,0);
        disconnect_client();
        
    }
    else if( strcmp(recieved,"ls") == EQUAL_STR ){
        list_files();
    }
    else if( strcmp(recieved,"") == EQUAL_STR ){
        disconnect_client();
    }
    else{
        send(client->sock_fd,"nok unknown command",20,0);
        fprintf(stderr, "%s|ERROR: Unknown message %s\n",client->client_id, recieved);
        disconnect_client();
    }
   
}

void disconnect_client(){
    close(client->sock_fd);
    client->sock_fd = -1;
    client->conn_state = IDLE;
    strcpy(hello_str,"hello ");
    printf("Client %s disconnected\n",client->client_id);
}

void init(){
    struct addrinfo *res;
    if( client == NULL ){
        client = malloc( sizeof(client_state) );
        client->server_addr = malloc( sizeof(char) * 16 );
        client->client_id = malloc( sizeof(char) * 16 );
    }
    gethostname( client->server_addr ,28);
    client->sock_fd = -1;
    client->conn_state = IDLE;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me
    getaddrinfo( NULL ,port_num , &hints, &res);
    socket_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    name.sin_family = AF_INET;
    name.sin_port = htons (PORT_NUM);
    name.sin_addr.s_addr = htonl (INADDR_ANY);
    bind (socket_fd, (struct sockaddr *) &name, sizeof (name));
    listen(socket_fd, 1);
    addr_size = sizeof(their_addr);
}



